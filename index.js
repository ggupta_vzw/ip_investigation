var ipaddr = require('ipaddr.js');
// var ipString = "::ffff:70.215.197.135" // mapped address - uidh address from the
// 6177337441 device
// var ipString = "70.215.197.135" //ipv4 address - ipv4
var ipString = "2001:db8:1234::1" //just ipv6 address
if (ipaddr.IPv4.isValid(ipString)) {
  // ipString is IPv4
  console.log("ipv4 address")
} else if (ipaddr.IPv6.isValid(ipString)) {
  var ip = ipaddr.IPv6.parse(ipString);
  if (ip.isIPv4MappedAddress()) {
    console.log("ipv4 mapped address")

    // ip.toIPv4Address().toString() is IPv4
  } else {
    console.log("ipv6 address")
    // ipString is IPv6
  }
} else {
  // ipString is invalid
}
